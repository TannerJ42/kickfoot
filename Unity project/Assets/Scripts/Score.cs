﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour 
{
    public static int Points = 0;
    int currentScore = 0;

    float timer = 0;
    public float TimeBetweenSaving = 1f;

	// Use this for initialization
	void Start () 
    {
        if (!PlayerPrefs.HasKey("points"))
        {
            PlayerPrefs.SetInt("points", Points);
        }
        Points = PlayerPrefs.GetInt("points");
        currentScore = Points;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (timer <= 0)
        {
            if (Points != currentScore)
            {
                PlayerPrefs.SetInt("points", Points);
                currentScore = Points;
            }
            timer = TimeBetweenSaving;
        }
        else
            timer -= Time.deltaTime;
	}

    void OnDestroy()
    {

    }
}
