﻿using UnityEngine;
using System.Collections;

public class footController : MonoBehaviour 
{
    public Direction direction = Direction.Left;
    bool Kicking = false;

	// Use this for initialization
	void Start () 
    {
	    
	}
	
	// Update is called once per frame
	void Update () 
    {
	    
	}

    public void StopKicking()
    {
        Kicking = false;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        Debug.Log("Hit!");
        if (c.tag == "ball")
        {
            c.SendMessage("GetKicked", direction);
            //if (direction == Direction.Left)
            //    c.SendMessage("kick_right");
            //if (direction == Direction.Right)
            //    c.SendMessage("kick_left");
        }
    }
}
