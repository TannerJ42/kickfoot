﻿using UnityEngine;
using System.Collections;

public class RotationFixer : MonoBehaviour 
{
    GameObject parentObject;

	// Use this for initialization
	void Start () 
    {
        parentObject = (GameObject)transform.parent.gameObject;
	}

    void LateUpdate()
    {
        var rotation = parentObject.transform.rotation.eulerAngles;
        rotation.x = rotation.y = 0;

        //rotation.z *= -1;
        transform.eulerAngles = -rotation;
        transform.localScale = new Vector3(-1, 1, 1);
    }
}
