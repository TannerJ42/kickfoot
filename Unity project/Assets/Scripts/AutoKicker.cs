﻿using UnityEngine;
using System.Collections;

public class AutoKicker : MonoBehaviour 
{
    PlayerController controller;

	// Use this for initialization
	void Start () 
    {
        controller = GetComponent<PlayerController>();


	}
	
	// Update is called once per frame
	void Update () 
    {
	    
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "ball")
        {
            BallController b = c.GetComponent<BallController>();

            if (b.currentDirection == Direction.Right)
            {
                controller.StartKicking();
            }
        }
    }
}
