﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreTextUpdater : MonoBehaviour 
{
    int currentPoints;
    Text text;

	// Use this for initialization
	void Start () 
    {

        text = GetComponent<Text>();

        UpdatePoints();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (currentPoints != Score.Points)
        {
            UpdatePoints();
        }

	}

    void UpdatePoints()
    {
        currentPoints = Score.Points;
        text.text = ("Points: " + currentPoints.ToString());
    }
}
