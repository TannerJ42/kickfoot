﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{

    Animator anim;

    bool PressedKick
    {
        get
        {
            return Input.GetKeyUp("enter");
        }

    }

    // Use this for initialization
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PressedKick)
            anim.SetTrigger("kick");
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "ball")
        {
            c.SendMessage("kick_left");
        }

        Debug.Log("hit");
    }
}
