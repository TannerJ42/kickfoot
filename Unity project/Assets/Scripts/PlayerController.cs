﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{

    Animator anim;
    public GameObject foot;

    public Direction facing = Direction.Right;

    bool PressedKick
    {
        get
        {
            if (Input.touchCount >= 1)
            {
                Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                Vector2 touchpos = new Vector2(wp.x, wp.y);

                if (collider2D == Physics2D.OverlapPoint(touchpos))
                    return true;
                else
                    return false;
            }
            return Input.GetKeyUp("space");
        }
    }

	// Use this for initialization
	void Start () 
    {
        anim = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        //if (PressedKick)
        //{
        //    StartKicking();
        //}
	}

    public void StartKicking()
    {
        foot.collider2D.enabled = true;
        if (facing == Direction.Right)
            anim.SetTrigger("kick");
        else if (facing == Direction.Left)
            anim.SetTrigger("kick_left");
    }

    public void StopKicking()
    {
        foot.collider2D.enabled = false;
    }
}
