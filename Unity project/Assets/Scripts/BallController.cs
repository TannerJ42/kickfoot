﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour 
{
    Animator anim;

    public Direction currentDirection = Direction.None;

	// Use this for initialization
	void Start () 
    {
        anim = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	    
	}

    public void GetKicked(Direction kickDirection)
    {
        Debug.Log("Getting kicked to the " + kickDirection);

        if (kickDirection == Direction.Left &&
            currentDirection != Direction.Left)
        {
            anim.SetTrigger("kick_left");
            currentDirection = Direction.Left;
        }
        if (kickDirection == Direction.Right &&
            currentDirection != Direction.Right)
        {
            anim.SetTrigger("kick_right");
            currentDirection = Direction.Right;
            Score.Points += 1;
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "ground")
            Destroy(this.gameObject, 1f);
    }

    void OnMouseDown()
    {
        Debug.Log("Clicked");
    }
}

public enum Direction
{
    None,
    Left,
    Right
}
