﻿using UnityEngine;
using System.Collections;

public class KickOnTouch : MonoBehaviour 
{
    PlayerController controller;

    bool PressedKick
    {
        get
        {
            if (Input.touchCount >= 1)
            {
                Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                Vector2 touchpos = new Vector2(wp.x, wp.y);

                if (collider2D == Physics2D.OverlapPoint(touchpos))
                    return true;
                else
                    return false;
            }
            return false;
        }
    }

    void OnMouseDown()
    {
        controller.StartKicking();
    }

	// Use this for initialization
	void Start () 
    {
        controller = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (PressedKick)
        {
            controller.StartKicking();
        }
	}
}
